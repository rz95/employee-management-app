# Employee Management App

This is a responsive frontend design of an Employee Management App built with Bootstrap 5.

Index Page:
![Index Page](/index.png "Index Page")

Dashboard:
![Dashboard](/dash.png "Dashboard")

Employees List:
![Employees List](/employees-list.png "Employees List")

You can see details of employees, edit, or delete them:

| Details | Edit | Delete |
| ------ | ------ | ------ |
| ![Employee Details](/Demo/employee-detail.png "Employee Details") | ![Employee Edit](/Demo/employee-edit.png "Employee Edit") | ![Employee Delete](/Demo/employee-delete.png "Employee Delete") |

Jobs List:
![Jobs List](/jobs-list.png "Jobs List")

You can see details of jobs, edit, or delete them:

| Details | Edit | Delete |
| ------ | ------ | ------ |
| ![Job Details](/Demo/job-detail.png "Job Details") | ![Job Edit](/Demo/job-edit.png "Job Edit") | ![Job Delete](/Demo/job-delete.png "Job Delete") |
